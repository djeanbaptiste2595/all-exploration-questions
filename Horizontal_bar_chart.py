# Complete the function horizontal_bar_chart below that takes in a string and creates
# a horizontal bar chart made out of the instances of the letters in that string based
# on the number of each letter.
# The return value is a list of the strings of the letters in alphabetical order.
# For example, given the sentence "abba has a banana", it would return this list
# of strings because there are seven "a", three "b", one "h", two "n",
# and one "s" letters in it:


# solution 1=

def horizontal_bar_chart(sentence):
    characters={}
    str2 = sentence.lower()
    for character in str2:
          if character.isalpha():
                if character in characters:
                  characters[character] += 1
                else:
                    characters[character] =  1
    list1 = []
    for k, v in characters.items():
        list1.append(k * v)
    list2 = sorted(list1)
    for i in list2:
        print(i)



sentence = "Yess"

print(horizontal_bar_chart(sentence))

#solution 2
def horizontal_bar_chart(sentence):
    res = []
    d = {}
    for char in sentence:
        if char.isalpha():
            if char not in d:
                d[char] = ''
            d[char] += char

    for k, v in d.items():
        res.append(v)
    return sorted(res)

# solution 3
def horizontal_bar_chart(sentence):
    dic = {}
    for item in sentence:
        if item >= "a" and item <= "z":
            if item not in dic:
                dic[item] = ""
            dic[item] += item
    answer = []
    for x in dic.values():
        result.append(x)
    return list(sorted(answer))

# solution 4

def horizontal_bar_chart(sentence):
    newstring=sorted(sentence)
    newlist = []
    templist = []
    for i in newstring:
        if i.isspace():
            pass
        elif len(templist) == 0:
            templist.append(i)
        elif i in templist:
            templist.append(i)
        else:
            item = ''.join(templist)
            newlist.append(item)
            templist.clear()
            templist.append(i)
    item = ''.join(templist)
    newlist.append(item)
    return newlist

solution 5
def horizontal_bar_chart (sentence):
  # make a dictionary
  letters_grouped_by_character = {}
  for letter in sentence:
    # if its a character (between a and z):
    is_between_a_and_z = letter >= "a" and letter <= "z"
    if is_between_a_and_z:
      # print(letter)
      # if the letter isnt in the dict"
      if letter not in letters_grouped_by_character:
        # print(letter)
        # add it
        letters_grouped_by_character[letter] = letter
      else:
        # concat to the existing string at that key
        letters_grouped_by_character[letter] = letters_grouped_by_character[letter] + letter

  # turn into a list
  result = sorted(letters_grouped_by_character.values())

  print(result)
  return result


# solution 6
def horizontal_bar_chart(sentence):
    d = {char: sentence.count(char) for char in sentence if char.isalpha()}
    return sorted([chars*k for k, chars in d.items()])

# solution 7
def horizontal_bar_chart(sentence):
    list = []
    for letter in sentence:
        if (sentence.count(letter)* letter) not in list and not letter.isspace():
            list.append(sentence.count(letter)* letter)
    list.sort()
    return list
